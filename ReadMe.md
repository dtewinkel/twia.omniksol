﻿# Omniksol interface driver

This project provides an driver and its NuGet package to communicate with an Omniksol solar power converter.

## Development

### Source Control

The (original) sources for this project are stored in a [GIT][] repository BitBucket: [twia.omniksol](https://bitbucket.org/dtewinkel/twia.Omniksol).
 
### Software
This project is built using:
 
* Visual Studio 2013.
* [NuGet][] 2.7.

Some tools that I found very useful:

* [ReSharper][] by [JetBrains][].
* [MarkdownPad 2](http://markdownpad.com/) for this file.

## Copyright and License

**Copyright © 2014, Daniël te Winkel, All rights reserved.**

[NuGet]: http://www.nuget.org/ "NuGet Package Manager"
[GIT]: http://git-scm.com/ "Git distributed source control system"
[JetBrains]: http://www.jetbrains.com
[ReSharper]: http://www.jetbrains.com/resharper/
