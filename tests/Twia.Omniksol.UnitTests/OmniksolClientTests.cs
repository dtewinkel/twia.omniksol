﻿using System;
using System.Net.Sockets;
using NUnit.Framework;
using Twia.ValidationTestHelper;

namespace Twia.Omniksol.UnitTests
{
   public class OmniksolClientTests
   {
      [Test]
      public void ConnectWithHostNameNullTest()
      {
         OmniksolClient client = new OmniksolClient();

// ReSharper disable AssignNullToNotNullAttribute
         ExceptionValidation.ValidateArgumentNullException(() => client.Connect(null), "hostName");
         ExceptionValidation.ValidateArgumentNullException(() => client.ConnectAsync(null), "hostName");
// ReSharper restore AssignNullToNotNullAttribute
      }

      [Test]
      public void ConnectWithHostNameEmptyTest()
      {
         OmniksolClient client = new OmniksolClient();

         ExceptionValidation.ValidateArgumentOutOfRangeException(() => client.Connect(""), "hostName", "");
         ExceptionValidation.ValidateArgumentOutOfRangeException(() => client.ConnectAsync(""), "hostName", "");
      }

      [Test]
      public void ConnectWithHostNameTooLongTest()
      {
         OmniksolClient client = new OmniksolClient();

         string hostName= new string('a', 256);

         ExceptionValidation.ValidateArgumentOutOfRangeException(() => client.Connect(hostName), "hostName", hostName);
         ExceptionValidation.ValidateArgumentOutOfRangeException(() => client.ConnectAsync(hostName), "hostName", hostName);
      }


      [Test]
      public void ConnectWithInvalidPortTest()
      {
         OmniksolClient client = new OmniksolClient();

         const string hostName = "localhost";
         const ushort port = 0;

         ExceptionValidation.ValidateArgumentOutOfRangeException(() => client.Connect(hostName, port), "port", port);
         ExceptionValidation.ValidateArgumentOutOfRangeException(() => client.ConnectAsync(hostName, port), "port", port);
      }


      [TestCase("::1")]
      [TestCase("bla bla bla")]
      public void ConnectWithInvalidHostnamesTests(string hostName)
      {
         OmniksolClient client = new OmniksolClient();

         ExceptionValidation.ValidateArgumentOutOfRangeException(() => client.Connect(hostName), "hostName", hostName);
      }


      [TestCase("::1")]
      [TestCase("bla bla bla")]
      public void ConnectAsyncWithInvalidHostnamesTests(string hostName)
      {
         OmniksolClient client = new OmniksolClient();

         ExceptionValidation.ValidateArgumentOutOfRangeException(() => client.ConnectAsync(hostName), "hostName", hostName);
      }


      [TestCase("10.2.12.257", Result = SocketError.HostNotFound)]
      [TestCase("not.really.existing", Result = SocketError.HostNotFound)]
      [TestCase("192.168.132.144", Result = SocketError.TimedOut)]
      [TestCase("microsoft.com", Result = SocketError.TimedOut)]
      [TestCase("localhost", Result = SocketError.ConnectionRefused)]
      public SocketError ConnectValidHostNamesTests(string hostName)
      {
         OmniksolClient client = new OmniksolClient();

         SocketException exception = Assert.Throws<SocketException>(() => client.Connect(hostName));
         return exception.SocketErrorCode;
      }


      [TestCase("10.2.12.257", Result = SocketError.HostNotFound)]
      [TestCase("not.really.existing", Result = SocketError.HostNotFound)]
      [TestCase("192.168.132.144", Result = SocketError.TimedOut)]
      [TestCase("microsoft.com", Result = SocketError.TimedOut)]
      [TestCase("localhost", Result = SocketError.ConnectionRefused)]
      public SocketError ConnectAsyncWithValidHostNamesTests(string hostName)
      {
         OmniksolClient client = new OmniksolClient();

         SocketException exception = Assert.Throws<SocketException>(async () => await client.ConnectAsync(hostName));
         return exception.SocketErrorCode;
      }
   }
}