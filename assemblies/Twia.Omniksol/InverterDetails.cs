﻿using JetBrains.Annotations;

namespace Twia.Omniksol
{
   /// <summary>
   /// Provides information about an inverter, such as serial numbers and firmware versions.
   /// </summary>
   /// <remarks>
   /// <para>
   /// Two serial numbers are available: the <see cref="InverterSerialNumber"/> and the <see cref="WifiSerialNumber"/>.
   /// </para>
   /// <para>
   /// Two firmware versions are available: the <see cref="MainFirmwareVersion"/> and the <see cref="SlaveFirmwareVersion"/>.
   /// </para>
   /// </remarks>
   public class InverterDetails
   {
      #region serial numbers.

      /// <summary>
      /// Property to get or set the serial number of the inverter.     
      /// </summary>
      /// <value>
      /// The serial number of the inverter. Can be <see langword="null"/> if unknown.
      /// </value>
      [CanBeNull]
      public string InverterSerialNumber { get; set; }

      /// <summary>
      /// Property to get or set the serial number of the Wifi module.     
      /// </summary>
      /// <value>
      /// The serial number of the Wifi module. Can be <see langword="null"/> if unknown.
      /// </value>
      [CanBeNull]
      public string WifiSerialNumber { get; set; }

      #endregion

      #region Firmware.

      /// <summary>
      /// Property to get or set the version of the main firmware.     
      /// </summary>
      /// <value>
      /// The version of the main firmware. Can be <see langword="null"/> if unknown.
      /// </value>
      public string MainFirmwareVersion { get; set; }

      /// <summary>
      /// Property to get or set the version of the slave firmware.     
      /// </summary>
      /// <value>
      /// The version of the slave firmware. Can be <see langword="null"/> if unknown.
      /// </value>
      public string SlaveFirmwareVersion { get; set; }

      #endregion
   }
}