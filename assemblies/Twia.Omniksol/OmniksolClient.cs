﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Twia.Validation;

namespace Twia.Omniksol
{
   /// <summary>
   /// Provides services to get data, or listen to events, from an Omniksol inverter
   /// </summary>
   /// <remarks>
   /// <para>
   /// A client can be used to actively pull information from an inverter.
   /// </para>
   /// <para>
   /// A client can also be used to listen to events sent by the inverter. 
   /// </para>
   /// </remarks>
   public class OmniksolClient
   {
      #region Constructors.

      #endregion

      #region Private fields.

      private ushort _port;
      private string _hostName;
      private TcpClient _client;
      private IPAddress _ipAddress;

      #endregion

      #region Private properties.

      #endregion

      #region Public properties.

      #endregion

      #region Public methods.

      /// <summary>
      /// Connect to a given host, setting all relevant connection parameters.
      /// </summary>
      /// <param name="hostName">The host name of the Omniksol inverter to connect to. Must be a valid host name or IP v4 address.</param>
      /// <param name="port">The port to connect to. Defaults to 8899.</param>
      /// <returns>The value <see langword="true"/> if a connection could be made, or <see langword="false"/> on error.</returns>
      public Task ConnectAsync([NotNull] string hostName, ushort port = 8899)
      {
         AssertAndSetHostNameAndPort(hostName, port);

         return ConnectAsync();
      }


      /// <summary>
      /// Connect to a given host, setting all relevant connection parameters.
      /// </summary>
      /// <param name="hostName">The host name of the Omniksol inverter to connect to. Must be a valid host name or IP v4 address.</param>
      /// <param name="port">The port to connect to. Defaults to 8899.</param>
      /// <returns>The value <see langword="true"/> if a connection could be made, or <see langword="false"/> on error.</returns>
      public void Connect([NotNull] string hostName, ushort port = 8899)
      {
         AssertAndSetHostNameAndPort(hostName, port);
         try
         {
            ConnectAsync().Wait();
         }
         catch (AggregateException ex)
         {
            // From the synchronous version we don't want to see the AggregateException, but the original InnerException.
            ExceptionDispatchInfo.Capture(ex.InnerException).Throw();
         }
      }

      #endregion

      #region Private methods.


      private Task ConnectAsync()
      {
         _client = new TcpClient(AddressFamily.InterNetwork);
         return _client.ConnectAsync(_ipAddress, _port);
      }

      private void AssertAndSetHostNameAndPort(string hostName, ushort port)
      {
         hostName.AssertIsNotNullOrEmpty("hostName");
         hostName.AssertMeetsCondition(val => val.Length > 0 && val.Length < 255, "hostName", "Length must be between 1 and 255 characters");
         port.AssertMeetsCondition(val => val > 0, "port", "Value must be in the range 1 - 65535");

         _hostName = hostName;
         _port = port;

         ValidateHostNameAndStoreIpAddress(_hostName);
      }


      private void ValidateHostNameAndStoreIpAddress([NotNull] string hostName)
      {
         // First check the basic type. We filter out IP v6 for instance here.
         UriHostNameType type = Uri.CheckHostName(hostName);
         switch (type)
         {
            case UriHostNameType.Dns:
            case UriHostNameType.IPv4:
               break;

            default:
               throw new ArgumentOutOfRangeException("hostName", hostName, "must be a valid IP v4 address or resolve to a valid IP v4 address");
         }

         // Now resolve the address to see if it really exists.
         _ipAddress = Dns.GetHostAddresses(hostName)
            .FirstOrDefault(address => address.AddressFamily == AddressFamily.InterNetwork);
      }

      #endregion
   }
}
